package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.dto.response.CandidateApplyResponse;
import com.example.RecruitmentManagement.dto.response.CandidateAssignedResponse;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface InterviewProcessRepository extends JpaRepository<InterviewProcess, Long> {
        // Tìm ngày phỏng vấn
        @Query(value = "SELECT i FROM interview_process i WHERE YEAR(i.time_interview)= :year " +
                        "AND MONTH(i.time_interview)= :month", nativeQuery = true)
        List<InterviewProcess> findByYearAndMonth(
                        @Param("year") Integer year, @Param("month") Integer month);

        // Get UserStatistics
        @Query(value = "SELECT COUNT(*) FROM interview_process WHERE process_status = :x1", nativeQuery = true)
        Integer getUserStatistics(@Param("x1") String x1);

        // Get UserBehaviors
        @Query(value = "SELECT COUNT(*) FROM interview_process " +
                        "WHERE applied_on = :x1 AND process_status = 'CV apply'", nativeQuery = true)
        Integer getUserBehaviors(@Param("x1") Date x1);

        // Get CandidateAssigned List
        @Query(value = "SELECT " +
                        "i.id AS id, j.id AS job_id, CONCAT_WS(' ', c.last_name, c.first_name) AS fullName," +
                        " i.time_interview AS timeInterview, e.score AS score, j.name AS jobName" +
                        " FROM interview_process AS i" +
                        " JOIN candidates AS c ON i.candidate_id = c.id" +
                        " JOIN evaluations AS e ON i.evaluation_id = e.id" +
                        " JOIN jobs AS j ON i.job_id = j.id" +
                        " WHERE i.interview_id = :id" +
                        " AND (:filterName IS NULL OR j.name = :filterName)" +
                        " AND ((:keyword IS NULL OR CONCAT_WS(' ', c.last_name, c.first_name) LIKE %:keyword%)" +
                        " OR (:keyword IS NULL OR i.time_interview LIKE %:keyword%)" +
                        " OR (:keyword IS NULL OR e.score LIKE %:keyword%)" +
                        " OR (:keyword IS NULL OR j.name LIKE %:keyword%))", nativeQuery = true)
        Page<CandidateAssignedResponse> searchAndFilterCandidateAssigned(
                        @Param("id") Long id, @Param("keyword") String keyword,
                        @Param("filterName") String filterName, Pageable pageable);

        // Get CandidateApplied List
        @Query(value = "SELECT " +
                "i.id AS id," +
                " j.id AS interviewer_id," +
                "CONCAT_WS(' ', c.last_name, c.first_name) AS candidateFullName," +
                " i.time_interview AS timeInterview," +
                " e.score AS score," +
                " CONCAT_WS(' ', j.last_name, j.first_name) AS interviewerFullName" +
                " FROM interview_process AS i" +
                " LEFT JOIN candidates AS c ON i.candidate_id = c.id" +
                " LEFT JOIN evaluations AS e ON i.evaluation_id = e.id" +
                " LEFT JOIN interviewers AS j ON i.interview_id = j.id" +
                " WHERE i.job_id = :id" +
                " AND ((:keyword IS NULL OR CONCAT_WS(' ', c.last_name, c.first_name) LIKE %:keyword%)" +
                " OR (:keyword IS NULL OR i.time_interview LIKE %:keyword%)" +
                " OR (:keyword IS NULL OR e.score LIKE %:keyword%)" +
                " OR (:keyword IS NULL OR CONCAT_WS(' ', j.last_name, j.first_name) LIKE %:keyword%))" +
                " AND (:filterName IS NULL OR CONCAT_WS(' ', j.last_name, j.first_name) = :filterName)", nativeQuery = true)
        Page<CandidateApplyResponse> searchAndFilterCandidateApplied(
                @Param("id") Long id, @Param("keyword") String keyword,
                @Param("filterName") String filterName, Pageable pageable);


        // Count how many candidates aplly to 1 job
        @Query(value = "SELECT COUNT(*) FROM interview_process  WHERE job_id = :id", nativeQuery = true)
        Integer countCandidateAppliedToJob(@Param("id") Long id);

        @Query(value = "SELECT COUNT(*) FROM interview_process " +
                        "WHERE process_status = 'CV apply'", nativeQuery = true)
        Integer countCandidate();

        @Query(value = "SELECT i.candidate_id FROM interview_process AS i WHERE i.id = :id", nativeQuery = true)
        Long getCandidateIdById(@Param("id") Long id);

        @Query(value = "SELECT i.evaluation_id FROM interview_process AS i WHERE i.id = :id", nativeQuery = true)
        Long getEvaluationIdById(@Param("id") Long id);
}
