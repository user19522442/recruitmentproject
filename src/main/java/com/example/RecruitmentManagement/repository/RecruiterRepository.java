package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Recruiter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RecruiterRepository extends JpaRepository<Recruiter, Long> {
    @Query(value = "SELECT COUNT(*) FROM recruiters", nativeQuery = true)
    Integer getCountRecruiters();

    @Query("SELECT r FROM Recruiter r " +
            "WHERE LOWER(r.firstName) LIKE LOWER(CONCAT('%', :keyword, '%')) " +
            "OR LOWER(r.lastName) LIKE LOWER(CONCAT('%', :keyword, '%'))")
    List<Recruiter> searchRecruitersByName(@Param("keyword") String keyword);
}
