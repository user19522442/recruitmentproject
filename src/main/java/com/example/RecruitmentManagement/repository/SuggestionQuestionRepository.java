package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.SuggestionQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SuggestionQuestionRepository extends JpaRepository<SuggestionQuestion, Long> {
    @Query("SELECT sq FROM SuggestionQuestion sq WHERE LOWER(sq.question) LIKE %:keyword%")
    List<SuggestionQuestion> searchSuggestionQuestionsByKeyword(@Param("keyword") String keyword);
}
