package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.HistoryApply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryApplyRepository extends JpaRepository<HistoryApply, Long> {

}
