package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
    @Query("SELECT j FROM Job j " +
            "WHERE (:type IS NULL OR j.type = :type) " +
            "AND (:workLocation IS NULL OR j.workLocation = :workLocation) " +
            "AND (:skillName IS NULL OR EXISTS (SELECT 1 FROM Skill s JOIN j.skills sk WHERE sk.skillName = :skillName)) " +
            "AND (:searchKeyword IS NULL OR " +
            "      LOWER(j.name) LIKE %:searchKeyword%)")
    Page<Job> filterJobs(@Param("type") String type,
                         @Param("workLocation") String workLocation,
                         @Param("skillName") String skillName,
                         @Param("searchKeyword") String searchKeyword,
                         Pageable pageable);
    @Query("SELECT COUNT(j) FROM Job j WHERE j.expired >= :today")
    int countJobsNotExpired(@Param("today") java.sql.Date today);
}
