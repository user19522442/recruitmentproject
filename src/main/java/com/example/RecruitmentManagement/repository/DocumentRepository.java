package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, Long> {
    Document findByName (String fileName);
}
