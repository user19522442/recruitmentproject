package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {
    @Query(value = "SELECT COUNT(*) FROM candidates", nativeQuery = true)
    Integer getCountCandidate();
}
