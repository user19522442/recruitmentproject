package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Interviewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InterviewerRepository extends JpaRepository<Interviewer, Long> {
    @Query(value = "SELECT COUNT(*) FROM interviewers", nativeQuery = true)
    Integer getCountInterviewers();

    @Query("SELECT r FROM Interviewer r " +
            "WHERE LOWER(r.firstName) LIKE LOWER(CONCAT('%', :keyword, '%')) " +
            "OR LOWER(r.lastName) LIKE LOWER(CONCAT('%', :keyword, '%'))")
    List<Interviewer> searchInterviewersByName(@Param("keyword") String keyword);
}
