package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.dto.EventParticipantDTO;
import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.entity.EventParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

public interface EventParticipantRepository extends JpaRepository<EventParticipant, Long> {
    @Query(value ="DELETE FROM event_eventparticipant ep WHERE ep.event.event_id = :eventId AND ep.candidate.candidate_id = :candidateId", nativeQuery = true)
    void deleteByEventIdAndCandidateId(@Param("eventId") Long eventId, @Param("candidateId") Long candidateId);
    @Query(value = "SELECT * FROM event_eventparticipant WHERE event_id = :eventId AND candidate_id = :candidateId", nativeQuery = true)
    Optional<EventParticipant> findByEventIdAndCandidateId(@Param("eventId") Long eventId, @Param("candidateId") Long candidateId);
}


