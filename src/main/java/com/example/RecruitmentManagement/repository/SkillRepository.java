package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
    List<Skill> findBySkillName(String skillName);
    List<Skill> findAllById(long skillId);
}
