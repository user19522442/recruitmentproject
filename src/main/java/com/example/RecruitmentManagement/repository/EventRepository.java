package com.example.RecruitmentManagement.repository;

import com.example.RecruitmentManagement.entity.Event;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    @Query(value = "SELECT count(*) FROM  event where CURRENT_DATE < time_event", nativeQuery = true)
    Integer countEvent();
    @Query(value = "SELECT event FROM Event event WHERE (:name is null or event.name LIKE %:name%) " +
            "and (:field is null or event.field = :field) "
            +"and (:address is null or event.address = : address) "
    )

    List<Event> findEventByNameAndFieldAndAddress(@Param("name") String name,
                                        @Param("field") String field,
                                        @Param("address") String address,
                                        Pageable pageable
    );

    @Query(value = "SELECT count(*) FROM event_eventparticipant where event_id = :event_id",nativeQuery = true)
    Integer countCandidateByEvent(@Param("event_id") Long event_id);
}