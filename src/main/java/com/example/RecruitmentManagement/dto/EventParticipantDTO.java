package com.example.RecruitmentManagement.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor

public class EventParticipantDTO {
    private Long event_id;
    private Long candidate_id;
}

