package com.example.RecruitmentManagement.dto.response;

import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleResponse {
    private List<InterviewProcess> interviewProcesses;
    private List<Event> events;
}
