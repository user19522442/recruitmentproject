package com.example.RecruitmentManagement.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBehaviorsResponse {
    private Integer today;
    private Integer yesterday;
    private Integer twoDaysAgo;
    private Integer threeDaysAgo;
    private Integer fourDaysAgo;
}
