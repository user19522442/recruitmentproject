package com.example.RecruitmentManagement.dto.response;

import com.example.RecruitmentManagement.entity.Candidate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GradingPageResponse {
    private Candidate candidate;
    private List<SuggestionQuestionResponse> suggestQuestions;
    private String url;
}
