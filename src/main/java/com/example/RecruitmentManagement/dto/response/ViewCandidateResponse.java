package com.example.RecruitmentManagement.dto.response;

import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.entity.Evaluation;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewCandidateResponse {
    private Candidate candidate;
    private Evaluation evaluation;
    private InterviewProcess interviewProcess;
    private String InterviewerName;
}
