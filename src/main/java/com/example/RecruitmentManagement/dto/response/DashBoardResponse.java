package com.example.RecruitmentManagement.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashBoardResponse {
    private Integer numberOfJobs;
    private Integer numberOfCandidates;
    private Integer numberOfEvents;
    private UserBehaviorsResponse userBehaviors;
    private UserStatisticsResponse userStatistics;
}
