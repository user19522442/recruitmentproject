package com.example.RecruitmentManagement.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserStatisticsResponse {
    private Integer cvApply;
    private Integer screeningCV;
    private Integer intitalInterview;
    private Integer ceoInterview;
    private Integer offered;
    private Integer hired;
}
