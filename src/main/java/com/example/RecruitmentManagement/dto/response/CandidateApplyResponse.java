package com.example.RecruitmentManagement.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CandidateApplyResponse {
    private Long id; // Interview Process ID
    private Long interviewer_id; // If avaliable
    private String candidateFullName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    private Date timeInterview;
    private String score;
    private String interviewerFullName;

}
