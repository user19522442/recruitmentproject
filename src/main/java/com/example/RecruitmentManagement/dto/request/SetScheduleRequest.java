package com.example.RecruitmentManagement.dto.request;

import com.example.RecruitmentManagement.entity.Interviewer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetScheduleRequest {
    private Long interviewProcessId;
    private java.util.Date timeInterview;
    private String location;
    private String platform;
    private Interviewer interviewer;
}
