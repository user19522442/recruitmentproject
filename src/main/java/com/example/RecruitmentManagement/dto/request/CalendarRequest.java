package com.example.RecruitmentManagement.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalendarRequest {
    private String startDateTime;
    private String endDateTime;
    private String email;
}
