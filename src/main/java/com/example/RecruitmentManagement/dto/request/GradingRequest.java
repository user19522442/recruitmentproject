package com.example.RecruitmentManagement.dto.request;

import com.example.RecruitmentManagement.entity.Evaluation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GradingRequest {
    private Evaluation evaluation;
    private String status;
}
