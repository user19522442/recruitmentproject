package com.example.RecruitmentManagement.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobRequest {
    private String name;
    private Date expired;
    private int salary;
    private int numberOfRecruits;
    private String type;
    private String jobPosition;
    private String exp;
    private String workLocation;
    private String description;
    private String shortIntroduction;
    private String requirement;
    private String benefit;
}
