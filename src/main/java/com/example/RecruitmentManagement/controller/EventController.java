package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.request.EventRequest;
import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/event")
public class EventController {

    @Autowired
    private EventService eventService;
    private static final String NOT_FOUND_MESSAGE = "An internal error has occurred";
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    // hien thi all event
    @GetMapping("/")
    public ResponseEntity<ApiResponse> getPagingEvents(@RequestParam(defaultValue = "0") int page) {
        try {
            Pageable pageable = PageRequest.of(page, 5);
            Page<Event> pageEvent = eventService.getPagingEvent(pageable);
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Get event successfully")
                    .data(pageEvent.getContent())
                    .build();
            return ResponseEntity.ok(apiResponse);
        } catch (Exception exception) {
            ApiResponse response = new ApiResponse(500, exception.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }

    }

    @GetMapping("/getAll")
    public ResponseEntity<ApiResponse> getAllEvent() {
        List<Event> eventList = eventService.getAll();
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get Events successfully")
                .data(eventList)
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }

    // hien thi event theo id
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> findEventById(@PathVariable("id") Long id) {
        try {
            Event event = eventService.findById(id);
            event.setNumber(eventService.countCandidateByEvent(id));
            ApiResponse response = new ApiResponse(200, "Get event information successfully", event);
            return ResponseEntity.ok(response);
        } catch (RuntimeException e) {
            ApiResponse response = new ApiResponse(404, e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, NOT_FOUND_MESSAGE, null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    // add event
    @PostMapping("/create")
    public ResponseEntity<ApiResponse> addEvent(@RequestPart("request") EventRequest request,
                                                @RequestPart(name = "file", required = false) MultipartFile file) {
        try {
            Event newEvent = eventService.addEvent(request,file);

            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Event was created successfully")
                    .data(newEvent)
                    .build();

            return ResponseEntity.status(HttpStatus.CREATED).body(apiResponse);
        } catch (IllegalArgumentException e) {
            ApiResponse response = new ApiResponse(400, e.getMessage(), null);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, "An internal server error occurred", null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> update(
            @PathVariable Long id,
            @RequestPart("request") EventRequest request,
            @RequestPart(name = "file", required = false) MultipartFile file

    ) {
        try {
            Event updatedEvent = eventService.updateEvent(id, request, file);

            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Event was updated successfully")
                    .data(updatedEvent)
                    .build();
            return ResponseEntity.ok(apiResponse);
        } catch (IllegalArgumentException e) {
            ApiResponse response = new ApiResponse(404, e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        } catch (RuntimeException e) {
            ApiResponse response = new ApiResponse(500, e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, "Could not save file", null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    @DeleteMapping ("/delete/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable("id") Long id) {
        try {
            eventService.removeEvent(id);
            ApiResponse response = new ApiResponse(200, "The event has been deleted successfully", null);
            return ResponseEntity.ok(response);
        } catch (RuntimeException e) {
            ApiResponse response = new ApiResponse(400, e.getMessage(), null);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, NOT_FOUND_MESSAGE, null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    @GetMapping("/filter")
    public ResponseEntity<ApiResponse> getJobByName(@RequestParam(defaultValue = "0") int page,
                                          @RequestParam String name,
                                          @RequestParam String field, @RequestParam String address)
    {
        List<Event> EventList = eventService.filterEvent(name, field,address, page);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get event successfully")
                .data(EventList)
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}

