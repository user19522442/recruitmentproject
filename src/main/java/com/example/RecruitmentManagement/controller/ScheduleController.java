package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.dto.response.ScheduleResponse;
import com.example.RecruitmentManagement.service.InterviewProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("")
public class ScheduleController {
    @Autowired
    private InterviewProcessService interviewProcessService;

    @GetMapping("/schedule")
    public ResponseEntity<ApiResponse> getSchedule(
            @RequestParam Integer month, @RequestParam Integer year) {

        ScheduleResponse schedule = new ScheduleResponse(
                interviewProcessService.getInterviewProcessesByTimeInterview(year, month),
                null);
        // Get event month and year

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Homepage got successful")
                .data(schedule)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);

    }
}
