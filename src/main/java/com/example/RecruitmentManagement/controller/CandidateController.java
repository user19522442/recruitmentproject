package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/candidate")
public class CandidateController {
    @Autowired
    private CandidateService candidateService;

    private static final String NOT_FOUND_MESSAGE = "An internal error has occurred";

    @GetMapping()
    public ResponseEntity<ApiResponse> getPagingCandidate(@RequestParam(defaultValue = "0") int page){
        try {
            Pageable pageable = PageRequest.of(page, 5);
            Page<Candidate> pageJob = candidateService.getPagingCandidate(pageable);

            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Get candidate successfully")
                    .data(pageJob.getContent())
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
        catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ApiResponse(500, exception.getMessage(), "")
            );
        }
    }

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> addCandidate(@RequestBody Candidate candidate){
        candidateService.addCandidate(candidate);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Candidate created successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateCandidate(@RequestBody Candidate candidate, @PathVariable("id") Long id){
        candidateService.updateCandidate(candidate, id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Candidate updated successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> deleteCandidate(@PathVariable("id") Long id){
        try {
            candidateService.deleteCandidate(id);
            ApiResponse response = new ApiResponse(200, "The Candidate has been deleted successfully", null);
            return ResponseEntity.ok(response);
        } catch (RuntimeException e) {
            ApiResponse response = new ApiResponse(400, e.getMessage(), null);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, NOT_FOUND_MESSAGE, null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{id}")
    public  ResponseEntity<ApiResponse> getCandidateById(@PathVariable("id")  Long id) {
        try {
            Candidate existingCandidate = candidateService.getCandidateById(id);

            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Get candidate by id successful")
                    .data(existingCandidate)
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } catch (RuntimeException e) {
            ApiResponse response = new ApiResponse(404, e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, NOT_FOUND_MESSAGE, null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/getall")
    public  ResponseEntity<ApiResponse> getAllCandidate(){
        List<Candidate> candidateList = candidateService.getAllCandidates();

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Get all candidate successful")
                .data(candidateList)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}
