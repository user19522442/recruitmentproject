package com.example.RecruitmentManagement.controller;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.dto.request.CalendarRequest;
import com.example.RecruitmentManagement.service.CalendarService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/calendar")
public class CalendarController {
    @Autowired
    private CalendarService calendarService;
    private JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private String gg_code;
    private static final String APPLICATION_NAME = "Google Calendar Recruitment App";
    @Value("${spring.security.oauth2.client.registration.google.redirect-uri}") // Đường dẫn callback URI của ứng dụng web
    private String redirectUri;
    @GetMapping("/authorization-url")
    public void getAuthorizationUrl(HttpServletResponse httpServletResponse) throws IOException, GeneralSecurityException {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        String authorizationUrl = calendarService.getAuthorizationUrl(HTTP_TRANSPORT, JSON_FACTORY, redirectUri);

        // Thực hiện chuyển hướng đến authorizationUrl
        httpServletResponse.sendRedirect(authorizationUrl);
    }
    @GetMapping("/Callback") // Endpoint callback, URL này phải khớp với đường dẫn callbackUri của ứng dụng web
    public ResponseEntity<ApiResponse> callback(@RequestParam("code") String code, HttpServletRequest request) {
        gg_code = code;
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Google account verification completed.")
                .data(gg_code)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PostMapping ("/schedule")
    public ResponseEntity<ApiResponse> Schedule(@RequestBody CalendarRequest calendarRequest, HttpServletResponse httpServletResponse) throws IOException, GeneralSecurityException {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

        Credential credential = calendarService.getCredentials(HTTP_TRANSPORT, JSON_FACTORY, gg_code, redirectUri);
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
        Event event = calendarService.createEvent(calendarRequest.getStartDateTime(), calendarRequest.getEndDateTime(), calendarRequest.getEmail());

        String calendarId = "primary";
        event = service.events().insert(calendarId, event).setSendNotifications(true).setConferenceDataVersion(1).execute();
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Meeting created successfully.")
                .data(event)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
