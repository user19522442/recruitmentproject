package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.dto.request.AuthRequest;
import com.example.RecruitmentManagement.dto.request.ForgotPasswordRequest;
import com.example.RecruitmentManagement.entity.Account;
import com.example.RecruitmentManagement.service.AccountService;
import com.example.RecruitmentManagement.service.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;
    @PostMapping("/create") //Thêm một tài khoản với sẽ tự động tạo ra user tương ứng theo role
    public ResponseEntity<ApiResponse> addAccount(@RequestBody Account account) {
        ApiResponse apiResponse = accountService.addAccount(account);
        return new ResponseEntity<>(apiResponse, HttpStatus.valueOf(apiResponse.getStatusCode()));
    }

    @GetMapping("/confirm/{token}") //Xác nhận đăng ký tài khoản thông qua email
    public ResponseEntity<ApiResponse> confirmAccount(@PathVariable("token") String token) {
        accountService.confirmAccount(token);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Account was confirmed successfully")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateAccount(
            @RequestBody Account account,
            @PathVariable("id") Long id
    ) {
        accountService.updateAccount(account, id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Account was updated successfully")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN') OR hasAuthority('ROLE_RECRUITER')")
    public ResponseEntity<ApiResponse> deleteAccount(
            @PathVariable("id") Long id )
    {
        accountService.deleteAccount(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Account was deleted successfully")
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN') OR hasAuthority('ROLE_RECRUITER')")
    public ResponseEntity<ApiResponse> getAccount(
            @PathVariable("id") Long id)
    {
        Account account =  accountService.getAccount(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .data(account)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority('ROLE_ADMIN') OR hasAuthority('ROLE_RECRUITER')")
    public ResponseEntity<ApiResponse> getListAccounts()
    {
        List<Account> accounts =  accountService.getListAccounts();

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .data(accounts)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
    @PostMapping("/authenticate") //Xác thực tài khoản đăng nhập
    public ResponseEntity<ApiResponse> authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
            Account existingAccount = accountService.findAccountByUsername(authRequest.getUsername());
            if (existingAccount.isEnabled() && authentication.isAuthenticated()) {
                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .data(jwtService.generateToken(authRequest.getUsername(), existingAccount.getId(), existingAccount.getRole()))
                        .build();
                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
            } else {
                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.BAD_REQUEST.value())
                        .message("Unconfirmed account!")
                        .build();
                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
            }
        } catch (BadCredentialsException ex) {
            // Xử lý ngoại lệ khi nhập sai mật khẩu hoặc tên đăng nhập
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.UNAUTHORIZED.value())
                    .message("Invalid username or password!")
                    .build();
            return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
        } catch (AuthenticationException ex) {
            // Xử lý ngoại lệ xác thực khác (nếu có)
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.UNAUTHORIZED.value())
                    .message("Authentication failed!")
                    .build();
            return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
        } catch (Exception ex) {
            // Xử lý các lỗi khác (nếu có)
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .message("An error occurred.")
                    .build();
            return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
        }
    }

    @PostMapping("/forgot-password") // Nhập mail để tiến hành nhận link đổi mật khẩu mới
    public ResponseEntity<ApiResponse> processForgotPassword(@RequestBody ForgotPasswordRequest request) {
        String userEmail = request.getEmail();
        if (accountService.requestPasswordReset(userEmail))
        {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Successful request!")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
        else
        {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message("Email not exists!")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
    }
    @PostMapping("/reset-password/{token}") // Xác nhận token và tiến hành tạo mật khẩu mới
    public ResponseEntity<ApiResponse> confirmPasswordReset(@PathVariable("token") String token,
                                                  @RequestBody ForgotPasswordRequest request) {
        if (accountService.confirmPasswordReset(token))
        {
            accountService.resetPassword(request.getPassword(), request.getEmail());
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("The password was reset!")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
        else
        {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message("Token not found!")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
    }
}
