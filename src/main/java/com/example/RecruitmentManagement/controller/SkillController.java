package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Skill;
import com.example.RecruitmentManagement.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/skills")
public class SkillController {
    @Autowired
    private SkillService skillService;

    @PostMapping()
    public ResponseEntity<ApiResponse> addSKill(@RequestBody Skill skill) {
        Skill newSkill = skillService.addSkill(skill);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Skill was created successfully")
                .data(newSkill)
                .build();

        return ResponseEntity.status(HttpStatus.CREATED).body(apiResponse);
    }

    @GetMapping()
    public ResponseEntity<ApiResponse> getSkills() {
        List<Skill> skills = skillService.getSkill();

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .data(skills)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getSkillById(@PathVariable Long id) {
        Skill skill = skillService.getSKillById(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get skill with ID: " + id + " deleted successfully")
                .data(skill)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> removeSkill(@PathVariable Long id) {
        skillService.deleteSkill(id);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Skill was deleted successfully")
                .data(null)
                .build();

        return ResponseEntity.ok(apiResponse);
    }
}