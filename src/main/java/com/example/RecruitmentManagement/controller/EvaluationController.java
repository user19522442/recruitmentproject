package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Evaluation;
import com.example.RecruitmentManagement.service.EvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/Evaluation")
public class EvaluationController {
    @Autowired
    private EvaluationService evaluationService;

    private static final String EVALUATION_MESSAGE = "Evaluation id: ";

    /*create an evaluation*/
    @PostMapping("/create")
    public ResponseEntity<ApiResponse> createEvaluation(@RequestBody Evaluation evaluation){
        evaluationService.createEvaluation(evaluation);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Add Evaluation success!!!")
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    /*get all evaluation*/
    @GetMapping
    public ResponseEntity<ApiResponse> getAllEvaluation(){
        if(evaluationService.getAllEvaluation() != null){
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("All Evaluation")
                    .data(evaluationService.getAllEvaluation())
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
        else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Don't have Evaluation")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
    }

    /*get evaluation by id*/
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getEvaluationByID(@PathVariable("id") Long id) {
        if (evaluationService.getEvaluationByID(id) != null) {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Evaluation with id: " + id)
                    .data(evaluationService.getEvaluationByID(id))
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(EVALUATION_MESSAGE + id + " is not found")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        }
    }

    /*delete evaluation*/
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> deleteEvaluationByID(@PathVariable("id") Long id) {
        if (evaluationService.deleteEvaluation(id)) {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message(EVALUATION_MESSAGE + id + " deleted")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(EVALUATION_MESSAGE + id + " is not found")
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        }

    }

    /*update an evaluation*/
    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateEvaluationByID(
            @RequestBody Evaluation evaluation,
            @PathVariable("id") Long id) {
        evaluationService.updateEvaluation(evaluation, id);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message(EVALUATION_MESSAGE + id + " is updated")
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
