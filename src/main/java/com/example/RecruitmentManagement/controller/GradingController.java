package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.request.GradingRequest;
import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.dto.response.GradingPageResponse;
import com.example.RecruitmentManagement.dto.response.SuggestionQuestionResponse;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import com.example.RecruitmentManagement.entity.SuggestionQuestion;
import com.example.RecruitmentManagement.service.EvaluationService;
import com.example.RecruitmentManagement.service.InterviewProcessService;
import com.example.RecruitmentManagement.service.SuggestionQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/grading")
public class GradingController {
        @Autowired
        private InterviewProcessService interviewProcessService;

        @Autowired
        private EvaluationService evaluationService;

        @Autowired
        private SuggestionQuestionService suggestionQuestionService;

        private static final String SUCCESS_MESSAGE = "An internal error has occurred";

        @GetMapping("")
        public ResponseEntity<ApiResponse> getGradingList(@RequestParam(defaultValue = "0") int page,
                                                          @RequestParam(defaultValue = "10") int size,
                                                          @RequestParam Long id,
                                                          @RequestParam(required = false) String keyword,
                                                          @RequestParam(required = false) String filterName) {
                Pageable pageable = PageRequest.of(page, size);
                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Grading List got successful")
                        .data(interviewProcessService
                                .getCandidateAssignedList(id, keyword, filterName, pageable)
                                .getContent())
                        .build();

                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }

        @GetMapping("/question")
        public ResponseEntity<ApiResponse> getAllSuggestionQuestions() {
                List<SuggestionQuestionResponse> suggestionQuestions = suggestionQuestionService.getAllSuggestionQuestions();

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully retrieved all SuggestionQuestions.")
                        .data(suggestionQuestions)
                        .build();

                return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }

        @PostMapping("/question/{skillId}/add")
        public ResponseEntity<ApiResponse> addSuggestionQuestionToSkill(
                @PathVariable Long skillId,
                @RequestBody SuggestionQuestion suggestionQuestion
        ) {
                SuggestionQuestion addedSuggestionQuestion = suggestionQuestionService.addSuggestionQuestionToSkill(skillId, suggestionQuestion);

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("SuggestionQuestion was deleted successfully.")
                        .build();

                return ResponseEntity.ok(apiResponse);
        }

        @DeleteMapping("question/{suggestionQuestionId}")
        public ResponseEntity<ApiResponse> deleteSuggestionQuestion(@PathVariable Long suggestionQuestionId) {
                suggestionQuestionService.deleteSuggestionQuestion(suggestionQuestionId);

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("SuggestionQuestion was deleted successfully.")
                        .build();

                return ResponseEntity.ok(apiResponse);
        }

        @GetMapping("question/filter")
        public ResponseEntity<ApiResponse> filterSuggestionQuestionsBySkillName(@RequestParam String skillName) {
                List<SuggestionQuestionResponse> filteredSuggestionQuestions = suggestionQuestionService.filterSuggestionQuestions(skillName);

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully filtered SuggestionQuestions.")
                        .data(filteredSuggestionQuestions)
                        .build();

                return ResponseEntity.ok(apiResponse);
        }

        @GetMapping("question/search")
        public ResponseEntity<ApiResponse> searchSuggestionQuestions(@RequestParam String keyword) {
                List<SuggestionQuestionResponse> searchedSuggestionQuestions = suggestionQuestionService.searchSuggestionQuestions(keyword);

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully searched SuggestionQuestions by keyword.")
                        .data(searchedSuggestionQuestions)
                        .build();

                return ResponseEntity.ok(apiResponse);
        }

        @GetMapping("/candidate-detail/{id}")
        public ResponseEntity<ApiResponse> viewCandidateDetail(@PathVariable Long id,
                                                               @RequestParam Long job_id) {
                InterviewProcess interviewProcess = interviewProcessService.getInterviewProcessById(id);
                GradingPageResponse gradingPage = new GradingPageResponse(
                        interviewProcess.getCandidate(),
                        suggestionQuestionService.getSuggestionQuestionsByJob(job_id),
                        interviewProcess.getCvInterview());
                // Get SuggestQuestions with Job_ID

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message(SUCCESS_MESSAGE)
                        .data(gradingPage)
                        .build();

                return ResponseEntity.ok(apiResponse);
        }

        @PutMapping("/candidate-detail/{id}/grade")
        public ResponseEntity<ApiResponse> gradingCandidate(@RequestBody GradingRequest gradingRequest,
                                                            @PathVariable Long id) {
                InterviewProcess interviewProcess = interviewProcessService.getInterviewProcessById(id);
                evaluationService.updateEvaluation(gradingRequest.getEvaluation(), interviewProcess.getEvaluation().getId());
                interviewProcessService.updateProcessStatus(gradingRequest.getStatus(), id);

                ApiResponse apiResponse = ApiResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message(SUCCESS_MESSAGE)
                        .build();

                return ResponseEntity.ok(apiResponse);
        }
}