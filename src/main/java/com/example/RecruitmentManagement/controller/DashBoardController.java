package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.dto.response.DashBoardResponse;
import com.example.RecruitmentManagement.service.EventService;
import com.example.RecruitmentManagement.service.InterviewProcessService;
import com.example.RecruitmentManagement.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("")
public class DashBoardController {
    @Autowired
    private InterviewProcessService interviewProcessService;

    @Autowired
    private JobService jobService;

    @Autowired
    private EventService eventService;

    @GetMapping("/dashboard")
    public ResponseEntity<ApiResponse> getHomePageDetail() {
        DashBoardResponse dHomePage = new DashBoardResponse(
                jobService.countJobsNotExpired(),
                interviewProcessService.countCandidateIsApplying(),
                eventService.countEvent(),
                interviewProcessService.lastFiveAppiledList(),
                interviewProcessService.countProcessStatus());

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Homepage got successful")
                .data(dHomePage)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}
