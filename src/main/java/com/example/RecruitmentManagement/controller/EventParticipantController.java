package com.example.RecruitmentManagement.controller;
import com.example.RecruitmentManagement.dto.EventParticipantDTO;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.entity.EventParticipant;
import com.example.RecruitmentManagement.repository.CandidateRepository;
import com.example.RecruitmentManagement.repository.EventParticipantRepository;
import com.example.RecruitmentManagement.repository.EventRepository;
import com.example.RecruitmentManagement.service.EventParticipantService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/eventparticipant")
public class EventParticipantController {
    private final EventParticipantService eventParticipantService;
    private final EventParticipantRepository eventParticipantRepository;
    private final EventRepository eventRepository;
    private final CandidateRepository candidateRepository;

    public EventParticipantController(EventParticipantRepository eventParticipantRepository,
                                      EventRepository eventRepository,
                                      CandidateRepository candidateRepository,
                                      EventParticipantService eventParticipantService) {
        this.eventParticipantRepository = eventParticipantRepository;
        this.eventRepository = eventRepository;
        this.candidateRepository = candidateRepository;
        this.eventParticipantService = eventParticipantService;
    }
    private static final String NOT_FOUND_MESSAGE = "An internal error has occurred";

    @PostMapping("/create")
    public ResponseEntity<String> addEventParticipant(@RequestBody EventParticipantDTO eventParticipantDTO) {
        Event event = eventRepository.findById(eventParticipantDTO.getEvent_id()).orElse(null);
        Candidate candidate = candidateRepository.findById(eventParticipantDTO.getCandidate_id()).orElse(null);
        if (event == null || candidate == null) {
            return ResponseEntity.badRequest().body("Invalid event_id or candidate_id.");
        }

        Optional<EventParticipant> existingEventParticipant = eventParticipantRepository.findByEventIdAndCandidateId(event.getId(), candidate.getId());
        if (existingEventParticipant.isPresent()) {
            throw new EntityNotFoundException("EventParticipant with event_id " + event.getId() + " and candidate_id " + candidate.getId() + " already exists.");
        }
        EventParticipant eventParticipant = new EventParticipant();
        eventParticipant.setEvent(event);
        eventParticipant.setCandidate(candidate);
        eventParticipantRepository.saveAndFlush(eventParticipant);
        return ResponseEntity.ok("Event participant added successfully.");
    }
    @DeleteMapping("delete/{eventId}/{candidateId}")
    public ResponseEntity<ApiResponse> deleteEventParticipant(@PathVariable Long eventId, @PathVariable Long candidateId) {
        try {
            eventParticipantService.deleteEventParticipant(eventId,candidateId);
            ApiResponse response = new ApiResponse(200, "The event_participant has been deleted successfully", null);
            return ResponseEntity.ok(response);
        } catch (RuntimeException e) {
            ApiResponse response = new ApiResponse(400, e.getMessage(), null);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            ApiResponse response = new ApiResponse(500, NOT_FOUND_MESSAGE, null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }

    }
}
