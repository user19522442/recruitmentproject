package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Admin;
import com.example.RecruitmentManagement.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/Admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    private static final String ADMIN_NOT_FOUND_MESSAGE = "Job with id %s does not exist";

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> createAdmin(@RequestBody Admin admin) {
        adminService.createAdmin(admin);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Add Admin success!!!")
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    /* get all evaluation */
    @GetMapping
    public ResponseEntity<ApiResponse> getAllAdmin() {
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("All Admin")
                .data(adminService.getAllAdmin())
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    /* get evaluation by id */
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getAdminByID(@PathVariable("id") Long id) {
        if (adminService.getAdminByID(id) != null) {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Admin with id: " + id)
                    .data(adminService.getAdminByID(id))
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(String.format(ADMIN_NOT_FOUND_MESSAGE, id))
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        }
    }

    /* delete evaluation */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> deleteAdminByID(@PathVariable("id") Long id) {
        if (adminService.deleteAdmin(id)) {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message(String.format(ADMIN_NOT_FOUND_MESSAGE, id))
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(String.format(ADMIN_NOT_FOUND_MESSAGE, id))
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        }

    }

    /* update an evaluation */
    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateAdminByID(
            @RequestBody Admin admin,
            @PathVariable("id") Long id) {
        adminService.updateAdmin(admin, id);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Admin id: " + id + " is updated")
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    /* get admin by pagination */
    @GetMapping("/pagination")
    public ResponseEntity<ApiResponse> getAllAdminPagination(@RequestParam int page) {
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("All Admin")
                .data(adminService.findAdminWithPagination(page))
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}
