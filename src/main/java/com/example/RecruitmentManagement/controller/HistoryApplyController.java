package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.HistoryApply;
import com.example.RecruitmentManagement.service.HistoryApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/historyApplies")
public class HistoryApplyController {
    private final HistoryApplyService historyApplyService;

    @Autowired
    public HistoryApplyController(HistoryApplyService historyApplyService) {
        this.historyApplyService = historyApplyService;
    }

    @GetMapping()
    public ResponseEntity<ApiResponse> getPagingHistoryApply(@RequestParam(defaultValue = "0") int page) {
        try {
            Pageable pageable = PageRequest.of(page, 5);
            Page<HistoryApply> pageJob = historyApplyService.getPagingHistoryApply(pageable);

            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Get historyApply successfully")
                    .data(pageJob.getContent())
                    .build();
            return ResponseEntity.ok(apiResponse);
        } catch (Exception exception) {
            ApiResponse response = new ApiResponse(500, exception.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping(value = {"/{id}"})
    public ResponseEntity<ApiResponse> getSkills(@PathVariable(required = false) Long id) {
        HistoryApply historyApply = historyApplyService.getHistoryApplyById(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .data(historyApply)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
