package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Interviewer;
import com.example.RecruitmentManagement.service.InterviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/interviewer")
public class InterviewerController {
    @Autowired
    private InterviewerService interviewerService;
    private static final String INTERVIEWER_NOT_FOUND_MESSAGE = "Interviewer not found with id: ";

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> createInterviewer(@RequestBody Interviewer interviewer) {
        interviewerService.createInterviewer(interviewer);
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Interviewer created successfully")
                .data(null)
                .build();
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateInterviewer(@RequestBody Interviewer interviewer, @PathVariable("id") Long id) {
        Interviewer existingInterviewer = interviewerService.getInterviewer(id);
        if (existingInterviewer == null) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(INTERVIEWER_NOT_FOUND_MESSAGE + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        interviewerService.updateInterviewer(interviewer, id);

        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Interviewer updated successfully")
                .data(null)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> deleteInterviewer(@PathVariable("id") Long id) {
        Interviewer existingInterviewer = interviewerService.getInterviewer(id);
        if (existingInterviewer == null) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(INTERVIEWER_NOT_FOUND_MESSAGE + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        interviewerService.deleteInterviewer(id);
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Interviewer deleted successfully")
                .data(null)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getInterviewer(@PathVariable("id") Long id) {
        Interviewer interviewer = interviewerService.getInterviewer(id);
        if (interviewer == null) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(INTERVIEWER_NOT_FOUND_MESSAGE + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Interviewer retrieved successfully")
                .data(interviewer)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<ApiResponse> getAllInterviewers() {
        List<Interviewer> interviewers = interviewerService.getListInterviewers();
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Interviewers retrieved successfully")
                .data(interviewers)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping("/count")
    public ResponseEntity<ApiResponse> getCountInterviewers() {
        Integer count = interviewerService.getCountInterviewers();
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Count of interviewers retrieved successfully")
                .data(count)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<ApiResponse> searchInterviewers(@RequestParam("keyword") String keyword) {
        List<Interviewer> searchResults = interviewerService.searchInterviewersByName(keyword);

        if (searchResults.isEmpty()) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message("No interviewers found with the given search term: " + keyword)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Interviewers found successfully")
                .data(searchResults)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
