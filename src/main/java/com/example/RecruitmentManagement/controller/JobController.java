package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.request.JobRequest;
import com.example.RecruitmentManagement.dto.request.SetScheduleRequest;
import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.dto.response.CandidateApplyResponse;
import com.example.RecruitmentManagement.dto.response.ViewCandidateResponse;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.entity.Evaluation;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import com.example.RecruitmentManagement.entity.Job;
import com.example.RecruitmentManagement.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/jobs")
public class JobController {
    @Autowired
    private JobService jobService;

    @Autowired
    private InterviewProcessService interviewProcessService;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private EvaluationService evaluationService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private InterviewerService interviewerService;

    private static final String NOT_FOUND_MESSAGE = "An internal error has occurred";

    // Get All Job
    @GetMapping()
    public ResponseEntity<ApiResponse> getPagingJob(@PageableDefault(size = 10, page = 0) Pageable pageable) {
        Page<Job> pageJob = jobService.getPagingJob(pageable);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get job successfully")
                .data(pageJob.getContent())
                .build();
        return ResponseEntity.ok(apiResponse);
    }

    // Get By ID
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getJobById(@PathVariable Long id) {
        Job newJob = jobService.getJobById(id);
        newJob.setCandidateIsApply(interviewProcessService.countJobQuantity(id));
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get job was with ID = " + id + " was successfully")
                .data(newJob)
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }

    @GetMapping("/getAll")
    public ResponseEntity<ApiResponse> getAllJob() {
        List<Job> jobList = jobService.getAll();
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get jobs successfully")
                .data(jobList)
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }

    // Add new Job
    @PostMapping()
    public ResponseEntity<ApiResponse> addJob(@RequestPart("requestJob") JobRequest requestJob,
                                              @RequestParam(name = "skillIds") List<Long> skillIds,
                                              @RequestPart(name = "file") MultipartFile file) {
        Job newJob = jobService.addJob(requestJob, new HashSet<>(skillIds), file);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Job was created successfully")
                .build();
        return ResponseEntity.status(HttpStatus.CREATED).body(apiResponse);
    }

    // Update Job
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> updateJob(@PathVariable Long id,
                                                 @RequestPart(required = false) JobRequest requestJob,
                                                 @RequestParam(required = false, name = "skillIds") Set<Long> skillIds,
                                                 @RequestPart(required = false, name = "file") MultipartFile file) {
        Job newJob = jobService.updateJob(id, requestJob, skillIds, file);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Job was updated successfully")
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }

    // Delete Job
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteJob(@PathVariable Long id) {
        jobService.deleteJob(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Job was delete successfully")
                .data(null)
                .build();

        return ResponseEntity.status(HttpStatus.CREATED).body(apiResponse);
    }

    // Filter
    @GetMapping("/search")
    public ResponseEntity<ApiResponse> filterJobs(@RequestParam(required = false) String type,
                                                  @RequestParam(required = false) String workLocation,
                                                  @RequestParam(required = false) String skillName,
                                                  @RequestParam(required = false) String searchKeyword,
                                                  @PageableDefault(size = 10, page = 0) Pageable pageable) {
        Page<Job> filteredJobs = jobService.filterJobs(type, workLocation, skillName, searchKeyword, pageable);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Filter Job successfully")
                .data(filteredJobs.getContent())
                .build();

        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }


    // xem candidate trong job đó
    @GetMapping("/{id}/candidates")
    public ResponseEntity<ApiResponse> getCandidate(@PathVariable Long id,
                                                    @RequestParam(required = false) String keyword,
                                                    @RequestParam(required = false) String filterName,
                                                    @PageableDefault(size = 10, page = 0) Pageable pageable) {
        Page<CandidateApplyResponse> candidateList = interviewProcessService.getCandidateAppliedList(id, keyword, filterName, pageable);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get candidateApplied list successfully")
                .data(candidateList.getContent())
                .build();
        return ResponseEntity.ok(apiResponse);
    }

    // Xem chi tiết một candidate trong danh sách apply trên
    @GetMapping("/{job_id}/candidate/{id}")
    public ResponseEntity<ApiResponse> getCandidateDetail(@PathVariable Long job_id,
                                                          @PathVariable Long id) {
        InterviewProcess interviewProcess = interviewProcessService.getInterviewProcessById(id);
        Candidate candidate = interviewProcess.getCandidate();
        Evaluation evaluation = interviewProcess.getEvaluation();
        String InterviewName = interviewProcess.getInterviewer().getFirstName() +
                               interviewProcess.getInterviewer().getLastName();


        ViewCandidateResponse viewCandidate = new ViewCandidateResponse(
                candidate, // lấy thông tin candidate
                evaluation, // Lấy đánh giá
                interviewProcess, // lấy quá trình phỏng vấn
                InterviewName // Tên người phỏng vấn
                );

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get candidateApplied Detail successfully")
                .data(viewCandidate)
                .build();
        return ResponseEntity.ok(apiResponse);
    }

    // Đặt lịch phỏng vấn
    @GetMapping("/{job_id}/candidate/{id}/set-schedule")
    public ResponseEntity<ApiResponse> scheduleInterview(
            @PathVariable Long job_id,
            @PathVariable Long id
    ){
        interviewerService.getListInterviewers();
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get set-schedule Successfully")
                .build();
        return ResponseEntity.ok(apiResponse);
    }

    @PostMapping("/{job_id}/candidate/{id}/set-schedule/confirm")
    public ResponseEntity<ApiResponse> confirmScheduleInterview(
            @PathVariable Long job_id,
            @PathVariable Long id,
            @RequestBody SetScheduleRequest scheduleRequest

            ){
         interviewProcessService.setSchedule(
                 scheduleRequest.getTimeInterview(),
                 scheduleRequest.getInterviewer(),
                 scheduleRequest.getInterviewProcessId()
         );


        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Confirm set-schedule Successfully")
                .build();
        return ResponseEntity.ok(apiResponse);
    }



    // Black list



    // Chấp nhận và từ chối


    // Job candidate nộp đơn
//    @PostMapping("/{jobId}/application")
//    public ResponseEntity<ApiResponse> uploadFile(@RequestParam("file") MultipartFile file,
//                                                  @PathVariable("jobId") Long jobId,
//                                                  @RequestBody Long candidateId
//    ) {
//
//        // Đoạn này chút làm
//        String fileUrl = documentService.uploadFile(file, candidateId);
//        interviewProcessService.createInterviewProcess(jobId, candidateId, fileUrl);
//
//        ApiResponse apiResponse = ApiResponse.builder()
//                .statusCode(HttpStatus.OK.value())
//                .message("Post application successfully")
//                .build();
//
//        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
//    }
}
