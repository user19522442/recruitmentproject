package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Skill;

import java.util.List;


public interface SkillService {
    Skill addSkill(Skill skill);
    List<Skill> getSkill();
    Skill getSKillById(Long skillId);
    void deleteSkill(Long skillId);
}
