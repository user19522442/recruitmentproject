package com.example.RecruitmentManagement.service;
import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.Account;

import java.util.List;

public interface AccountService {
    ApiResponse addAccount(Account account);
    void confirmAccount(String token);
    void updateAccount(Account account, Long id);
    void deleteAccount(Long id);
    Account getAccount(Long id);
    Account findAccountByUsername(String username);
    List<Account> getListAccounts();
    boolean requestPasswordReset(String userEmail);
    boolean confirmPasswordReset(String token);
    void resetPassword(String password, String userEmail);
}
