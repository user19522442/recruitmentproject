package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Recruiter;

import java.util.List;

public interface RecruiterService {
    void createRecruiter(Recruiter recruiter);
    void updateRecruiter(Recruiter recruiter, Long recruiterId);
    void deleteRecruiter(Long recruiterId);
    Recruiter getRecruiter(Long recruiterId);
    List<Recruiter> getListRecruiters();
    Integer getCountRecruiters();
    List<Recruiter> searchRecruitersByName(String keyword);
}
