package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Admin;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AdminService {
    public void createAdmin(Admin admin);
    public List<Admin> getAllAdmin();
    public Admin getAdminByID(Long id);
    public boolean deleteAdmin(Long id);
    public void updateAdmin(Admin admin, Long id);
    public Page<Admin> findAdminWithPagination(int page);
}
