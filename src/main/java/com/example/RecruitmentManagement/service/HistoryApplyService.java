package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.HistoryApply;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HistoryApplyService {
    HistoryApply addHistoryApply(HistoryApply historyApply);
    Page<HistoryApply> getPagingHistoryApply(Pageable pageable);
    HistoryApply getHistoryApplyById(Long id);
    void deleteHistoryApply(Long id);
}
