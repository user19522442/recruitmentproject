package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.HistoryApply;
import com.example.RecruitmentManagement.repository.HistoryApplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class HistoryApplyServiceImpl implements HistoryApplyService{
    private final HistoryApplyRepository historyApplyRepository;

    @Autowired
    public HistoryApplyServiceImpl(HistoryApplyRepository historyApplyRepository) {
        this.historyApplyRepository = historyApplyRepository;
    }


    // Unfinished
    @Override
    public HistoryApply addHistoryApply(HistoryApply historyApply) {
        return null;
    }

    @Override
    public Page<HistoryApply> getPagingHistoryApply(Pageable pageable) {
        return historyApplyRepository.findAll(pageable);
    }

    @Override
    public HistoryApply getHistoryApplyById(Long id) {
        return historyApplyRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("HistoryApply with id " + id + " does not exist"));
    }

    @Override
    public void deleteHistoryApply(Long id) {
        HistoryApply historyApply = historyApplyRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("HistoryApply with id " + id + " does not exist"));

        historyApplyRepository.delete(historyApply);
    }
}
