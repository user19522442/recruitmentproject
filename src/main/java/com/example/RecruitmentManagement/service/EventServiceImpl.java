package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.request.EventRequest;
import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.exception.FileStorageException;
import com.example.RecruitmentManagement.repository.EventRepository;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    String noteFoundMessage = "Could not save file: ";
    String eventMessage = "Event with id ";
    String doesNotExistMessage = " does not exist";

    @Autowired
    public  EventServiceImpl (EventRepository eventRepository)
    {
        this.eventRepository = eventRepository;
    }
    private boolean isImageFile(MultipartFile file) {
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        assert fileExtension != null;
        return Arrays.asList(new String[]{"png", "jpg", "jpeg", "bmp"})
                .contains(fileExtension.trim().toLowerCase());
    }

    @Autowired
    private Storage storage;
    private String bucketName;

    @Autowired
    public void FileUploadService(Storage storage, @Value("${firebase.storage.bucket}") String bucketName){
        this.storage = storage;
        this.bucketName = bucketName;
    }

    public String saveLinkImg(MultipartFile file) throws IOException{
        String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();

        BlobInfo blobInfo = BlobInfo.newBuilder("quanlytuyendung-b9f07.appspot.com", fileName)
                .setContentType(file.getContentType())
                .build();
        storage.create(blobInfo, file.getBytes());

        return String.format("https://firebasestorage.googleapis.com/v0/b/%s/o/%s?alt=media", bucketName, fileName);
    }
    @Override
    public Event addEvent(EventRequest eventDTO, MultipartFile file )
    {
        String fileName = file.getOriginalFilename();

        if (!isImageFile(file)) {
            throw new IllegalArgumentException("This isn't an image file. " + fileName);
        }
        try
        {
            Event event = new Event();
            event.setName(eventDTO.getName());
            event.setAddress(eventDTO.getAddress());
            event.setTimeEvent(eventDTO.getTimeEvent());
            event.setField(eventDTO.getField());
            event.setDescription(eventDTO.getDescription());
            event.setGuestInformation(eventDTO.getGuestInformation());
            event.setContact(eventDTO.getContact());
            event.setNumber(eventDTO.getNumber());
            event.setGuest(eventDTO.getGuest());
            event.setLinkImg(saveLinkImg(file));
            return eventRepository.saveAndFlush(event);
        } catch (IOException e)
        {
            throw new FileStorageException(noteFoundMessage + fileName, e);
        }
    }

    @Override
    public Event updateEvent(Long id, EventRequest eventDTO, MultipartFile file ) {
        Event event = eventRepository.findById(id).orElse(null);

        if (event == null) {
            throw new IllegalArgumentException(eventMessage + id + doesNotExistMessage);
        }

        if (file != null) {
            String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

            if (!isImageFile(file)) {
                throw new IllegalArgumentException("This isn't an image file. " + fileName);
            }

            try {
                if (fileName.contains("..")) {
                    throw new IllegalArgumentException("Filename contains invalid path sequence " + fileName);
                }
                event.setLinkImg(saveLinkImg(file));

            } catch (IOException e) {
                throw new FileStorageException(noteFoundMessage + fileName, e);
            }
        }
        event.setName(eventDTO.getName());
        event.setAddress(eventDTO.getAddress());
        event.setTimeEvent(eventDTO.getTimeEvent());
        event.setField(eventDTO.getField());
        event.setDescription(eventDTO.getDescription());
        event.setGuestInformation(eventDTO.getGuestInformation());
        event.setContact(eventDTO.getContact());
        event.setNumber(eventDTO.getNumber());
        event.setGuest(eventDTO.getGuest());
        return eventRepository.saveAndFlush(event);
    }
    @Override
    public void removeEvent(Long id) {
        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(eventMessage + id + doesNotExistMessage));
        eventRepository.delete(event);
    }
    @Override
    public Event findById(Long id) {
        return eventRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(eventMessage + id + doesNotExistMessage));
    }
    @Override
    public Page<Event> getPagingEvent(Pageable pageable) {
        return eventRepository.findAll(pageable);
    }
    @Override
    public List<Event> filterEvent(String name, String field, String address, int page) {

        // convert to null if the string is empty
        name = (name != null && !name.isEmpty()) ? name : null;
        field = (field != null && !field.isEmpty()) ? field : null;
        address = (address != null && !address.isEmpty()) ? address : null;
        Pageable pageable = PageRequest.of(page, 5);
        return (List<Event>) eventRepository.findEventByNameAndFieldAndAddress(name, field,address, pageable);
    }
    @Override
    public Integer countEvent(){
        return eventRepository.countEvent();
    }
    @Override
    public Integer countCandidateByEvent(Long event_id)
    {
        return eventRepository.countCandidateByEvent(event_id);
    }
    @Override
    public List<Event> getAll() {
        return eventRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }
}