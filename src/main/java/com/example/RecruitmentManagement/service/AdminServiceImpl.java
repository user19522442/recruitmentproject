package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Admin;
import com.example.RecruitmentManagement.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminRepository adminRepository;

    /* create an admin */
    @Override
    public void createAdmin(Admin admin) {
            adminRepository.save(admin);
    }

    @Override
    public List<Admin> getAllAdmin() {
        return adminRepository.findAll();
    }

    @Override
    public Admin getAdminByID(Long id) {
        return adminRepository.findById(id).orElseThrow(null);
    }

    @Override
    public boolean deleteAdmin(Long id) {
        Admin deleteAdmin = adminRepository.findById(id).orElse(null);
        if (deleteAdmin != null) {
            adminRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public void updateAdmin(Admin admin, Long id) {
        Admin existAdmin = adminRepository.findById(id).orElse(null);
        if(existAdmin != null) {
            existAdmin.setFirstName(admin.getFirstName());
            existAdmin.setLastName(admin.getLastName());
            existAdmin.setGender(admin.getGender());
            existAdmin.setDateOfBirth(admin.getDateOfBirth());
            existAdmin.setAddress(admin.getAddress());
            existAdmin.setPhone(admin.getPhone());
            adminRepository.save(existAdmin);
        }
    }

    @Override
    public Page<Admin> findAdminWithPagination(int page) {
        return adminRepository.findAll(PageRequest.of(page, 5));
    }
}
