package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Interviewer;
import com.example.RecruitmentManagement.repository.InterviewerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterviewerServiceImpl implements InterviewerService{
    @Autowired
    private InterviewerRepository interviewerRepository;
    @Override
    public void createInterviewer(Interviewer interviewer) {
        // Triển khai logic tạo Interviewer ở đây
        interviewerRepository.save(interviewer);
    }

    @Override
    public void updateInterviewer(Interviewer interviewer, Long interviewerId) {
        // Triển khai logic cập nhật Interviewer ở đây
        Interviewer existingInterviewer = interviewerRepository.findById(interviewerId)
                .orElseThrow(() -> new IllegalArgumentException("Interviewer not found with ID: " + interviewerId));

        existingInterviewer.setFirstName(interviewer.getFirstName());
        existingInterviewer.setLastName(interviewer.getLastName());
        existingInterviewer.setGender(interviewer.getGender());
        existingInterviewer.setDateOfBirth(interviewer.getDateOfBirth());
        existingInterviewer.setPhone(interviewer.getPhone());
        existingInterviewer.setAddress(interviewer.getAddress());

        interviewerRepository.save(existingInterviewer);
    }

    @Override
    public void deleteInterviewer(Long interviewerId) {
        // Triển khai logic xóa Interviewer ở đây
        interviewerRepository.deleteById(interviewerId);
    }

    @Override
    public Interviewer getInterviewer(Long interviewerId) {
        // Triển khai logic lấy thông tin Interviewer theo ID ở đây
        return interviewerRepository.findById(interviewerId)
                .orElseThrow(() -> new IllegalArgumentException("Interviewer not found with ID: " + interviewerId));
    }

    @Override
    public List<Interviewer> getListInterviewers() {
        // Triển khai logic lấy danh sách tất cả Interviewer ở đây
        return interviewerRepository.findAll();
    }

    @Override
    public Integer getCountInterviewers() {
        return interviewerRepository.getCountInterviewers();
    }

    @Override
    public List<Interviewer> searchInterviewersByName(String keyword) {
        return interviewerRepository.searchInterviewersByName(keyword);
    }
}
