package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Interviewer;

import java.util.List;

public interface InterviewerService {
    void createInterviewer(Interviewer interviewer);
    void updateInterviewer(Interviewer interviewer, Long interviewerId);
    void deleteInterviewer(Long interviewerId);
    Interviewer getInterviewer(Long interviewerId);
    List<Interviewer> getListInterviewers();
    Integer getCountInterviewers();
    List<Interviewer> searchInterviewersByName(String keyword);
}
