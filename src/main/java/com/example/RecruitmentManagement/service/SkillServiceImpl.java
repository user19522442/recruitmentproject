package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Skill;
import com.example.RecruitmentManagement.exception.DuplicateException;
import com.example.RecruitmentManagement.exception.NullException;
import com.example.RecruitmentManagement.exception.WarningException;
import com.example.RecruitmentManagement.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillServiceImpl implements SkillService {
    private final SkillRepository skillRepository;
    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Skill addSkill(Skill skill) {
        List<Skill> foundSkills = skillRepository.findBySkillName(skill.getSkillName());
        if (!foundSkills.isEmpty()) {
            throw new DuplicateException("Skill Name already exists.");
        }
        return skillRepository.saveAndFlush(skill);
    }

    @Override
    public List<Skill> getSkill() {
        return skillRepository.findAll();
    }

    @Override
    public Skill getSKillById(Long skillId) {
        Skill skill = skillRepository.findById(skillId).orElseThrow(
                () -> new NullException("Skill with ID " + skillId + " not found."));

        return skill;
    }

    @Override
    public void deleteSkill(Long skillId) {
        Skill skill = skillRepository.findById(skillId).orElseThrow(
                () -> new NullException("Skill with ID " + skillId + " not found."));

        if (!skill.getJobs().isEmpty()) {
            throw new WarningException("Cannot delete Skill with ID " + skillId + " because it has related Jobs.");
        }

        if (!skill.getSuggestionQuestionList().isEmpty()) {
            throw new WarningException("Cannot delete Skill with ID " + skillId + " because it has related SuggestionQuestions.");
        }

        skillRepository.deleteById(skillId);
    }
}
