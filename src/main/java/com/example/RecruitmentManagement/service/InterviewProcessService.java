package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.response.CandidateApplyResponse;
import com.example.RecruitmentManagement.dto.response.CandidateAssignedResponse;
import com.example.RecruitmentManagement.dto.response.UserBehaviorsResponse;
import com.example.RecruitmentManagement.dto.response.UserStatisticsResponse;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import com.example.RecruitmentManagement.entity.Interviewer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface InterviewProcessService {
        void createInterviewProcess(Long jobId, Long candidateId, String url);

        void deleteInterviewProcess(Long id);

        void setSchedule(java.util.Date timeSchedule, Interviewer interviewer, Long id);

        List<InterviewProcess> getInterviewProcessesByTimeInterview(Integer year, Integer month);

        UserStatisticsResponse countProcessStatus();

        UserBehaviorsResponse lastFiveAppiledList();

        Page<CandidateAssignedResponse> getCandidateAssignedList(
                        Long id, String keyword, String filterName, Pageable pageable);

        Page<CandidateApplyResponse> getCandidateAppliedList(
                        Long id, String keyword, String filterName, Pageable pageable);

        int countJobQuantity(Long id);

        Integer countCandidateIsApplying();

        InterviewProcess getInterviewProcessById(Long id);

        void updateProcessStatus(String update, Long id);
}
