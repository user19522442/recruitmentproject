package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.response.SuggestionQuestionResponse;
import com.example.RecruitmentManagement.entity.Job;
import com.example.RecruitmentManagement.entity.Skill;
import com.example.RecruitmentManagement.entity.SuggestionQuestion;
import com.example.RecruitmentManagement.repository.JobRepository;
import com.example.RecruitmentManagement.repository.SkillRepository;
import com.example.RecruitmentManagement.repository.SuggestionQuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SuggestionQuestionServiceImpl implements SuggestionQuestionService {

    private final SkillRepository skillRepository;
    private final SuggestionQuestionRepository suggestionQuestionRepository;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    public SuggestionQuestionServiceImpl(SkillRepository skillRepository, SuggestionQuestionRepository suggestionQuestionRepository) {
        this.skillRepository = skillRepository;
        this.suggestionQuestionRepository = suggestionQuestionRepository;
    }

    @Override
    public List<SuggestionQuestionResponse> getAllSuggestionQuestions() {
        List<SuggestionQuestion> suggestionQuestions = suggestionQuestionRepository.findAll();

        // Tạo một danh sách mới chứa SuggestionQuestionDTO, bao gồm thông tin SuggestionQuestion và tên của Skill
        List<SuggestionQuestionResponse> SuggestionQuestionDTO = new ArrayList<>();
        for (SuggestionQuestion suggestionQuestion : suggestionQuestions) {
            SuggestionQuestionResponse suggestionQuestionDTO = new SuggestionQuestionResponse();
            suggestionQuestionDTO.setQuestion(suggestionQuestion.getQuestion());
            suggestionQuestionDTO.setSkillName(suggestionQuestion.getSkill().getSkillName());

            SuggestionQuestionDTO.add(suggestionQuestionDTO);
        }
        return SuggestionQuestionDTO;
    }

    @Override
    public SuggestionQuestion addSuggestionQuestionToSkill(Long skillId, SuggestionQuestion suggestionQuestion) {
        Skill skill = skillRepository.findById(skillId).orElseThrow(
                () -> new IllegalArgumentException("Skill with ID " + skillId + " not found."));

        suggestionQuestion.setSkill(skill);
        suggestionQuestionRepository.save(suggestionQuestion);

        skill.getSuggestionQuestionList().add(suggestionQuestion);
        skillRepository.save(skill);

        return suggestionQuestion;
    }

    @Override
    public SuggestionQuestion updateSuggestionQuestion(Long suggestionQuestionId, SuggestionQuestion updatedSuggestionQuestion) {
        SuggestionQuestion existingSuggestionQuestion = suggestionQuestionRepository.findById(suggestionQuestionId).orElseThrow(
                () -> new IllegalArgumentException("SuggestionQuestion with ID " + suggestionQuestionId + " not found."));

        existingSuggestionQuestion.setQuestion(updatedSuggestionQuestion.getQuestion());

        existingSuggestionQuestion = suggestionQuestionRepository.save(existingSuggestionQuestion);

        return existingSuggestionQuestion;
    }

    @Override
    public void deleteSuggestionQuestion(Long suggestionQuestionId) {
        SuggestionQuestion suggestionQuestion = suggestionQuestionRepository.findById(suggestionQuestionId).orElseThrow(
                () -> new IllegalArgumentException("SuggestionQuestion with ID " + suggestionQuestionId + " not found."));

        suggestionQuestionRepository.delete(suggestionQuestion);
    }

    @Override
    public List<SuggestionQuestionResponse> filterSuggestionQuestions(String skillName) {
        List<SuggestionQuestion> suggestionQuestions = suggestionQuestionRepository.findAll();
        List<SuggestionQuestionResponse> filteredQuestions = new ArrayList<>();

        for (SuggestionQuestion suggestionQuestion : suggestionQuestions) {
            Skill skill = suggestionQuestion.getSkill();
            if (skill != null && skill.getSkillName().equals(skillName)) {
                SuggestionQuestionResponse suggestionQuestionDTO = new SuggestionQuestionResponse();
                suggestionQuestionDTO.setQuestion(suggestionQuestion.getQuestion());
                suggestionQuestionDTO.setSkillName(skill.getSkillName());
                filteredQuestions.add(suggestionQuestionDTO);
            }
        }

        return filteredQuestions;
    }

    @Override
    public List<SuggestionQuestionResponse> searchSuggestionQuestions(String keyword) {
        List<SuggestionQuestion> suggestionQuestions = suggestionQuestionRepository.searchSuggestionQuestionsByKeyword(keyword);
        List<SuggestionQuestionResponse> searchedQuestions = new ArrayList<>();

        for (SuggestionQuestion suggestionQuestion : suggestionQuestions) {
            SuggestionQuestionResponse suggestionQuestionDTO = new SuggestionQuestionResponse();
            suggestionQuestionDTO.setQuestion(suggestionQuestion.getQuestion());

            Skill skill = suggestionQuestion.getSkill();
            suggestionQuestionDTO.setSkillName(skill.getSkillName());

            searchedQuestions.add(suggestionQuestionDTO);
        }

        return searchedQuestions;
    }

    @Override
    public List<SuggestionQuestionResponse> getSuggestionQuestionsByJob(Long jobId) {
        Job job = jobRepository.findById(jobId).orElseThrow(
                () -> new IllegalArgumentException("Job with ID " + jobId + " not found."));

        Set<Skill> skills = job.getSkills();
        List<SuggestionQuestionResponse> suggestionQuestionList = new ArrayList<>();

        for (Skill skill: skills) {
            List<SuggestionQuestion> suggestionQuestions = skill.getSuggestionQuestionList();
            for (SuggestionQuestion item: suggestionQuestions) {
                SuggestionQuestionResponse dto = new SuggestionQuestionResponse();
                dto.setQuestion(item.getQuestion());
                suggestionQuestionList.add(dto);
            }
        }
        return suggestionQuestionList;
    }
}
