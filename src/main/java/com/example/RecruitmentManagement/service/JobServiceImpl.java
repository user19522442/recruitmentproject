package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.request.JobRequest;
import com.example.RecruitmentManagement.entity.Job;
import com.example.RecruitmentManagement.entity.Skill;
import com.example.RecruitmentManagement.exception.FileStorageException;
import com.example.RecruitmentManagement.exception.NullException;
import com.example.RecruitmentManagement.repository.InterviewProcessRepository;
import com.example.RecruitmentManagement.repository.JobRepository;
import com.example.RecruitmentManagement.repository.SkillRepository;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class JobServiceImpl implements JobService {
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private InterviewProcessRepository interviewProcessRepository;
    @Autowired
    private Storage storage;
    private String bucketName;
    @Autowired
    public void FileUploadService(Storage storage, @Value("${firebase.storage.bucket}") String bucketName) {
        this.storage = storage;
        this.bucketName = bucketName;
    }
    private static final String JOB_NOT_FOUND_MESSAGE = "Job with id %s does not exist";
    private static final String SKILL_NOT_FOUND_MESSAGE = "Skill with id %s does not exist";

    private boolean isImageFile(MultipartFile file) {
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        assert fileExtension != null;
        return Arrays.asList(new String[] { "png", "jpg", "jpeg", "bmp" })
                .contains(fileExtension.trim().toLowerCase());
    }

    public String savePosterJob(MultipartFile file) throws IOException {
        String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();

        BlobInfo blobInfo = BlobInfo.newBuilder("quanlytuyendung-b9f07.appspot.com", fileName)
                .setContentType(file.getContentType())
                .build();
        storage.create(blobInfo, file.getBytes());

        return String.format("https://firebasestorage.googleapis.com/v0/b/%s/o/%s?alt=media", bucketName, fileName);
    }

    @Override
    public Job addJob(JobRequest jobDTO, Set<Long> selectedSkillIds, MultipartFile file) {
        if (file != null && !isImageFile(file)) {
            throw new FileStorageException("Invalid image file. Only PNG, JPG, JPEG, and BMP are allowed.");
        }

        Job job = new Job();
        setNewJob(jobDTO, job);

        // Lấy danh sách các kỹ năng đã chọn
        Set<Skill> selectedSkills = selectedSkillIds.stream()
                .map(skillId -> skillRepository.findById(skillId).orElseThrow(
                        () -> new NullException(String.format(SKILL_NOT_FOUND_MESSAGE, skillId))
                ))
                .collect(Collectors.toSet());

        job.setSkills(selectedSkills);

        try {
            job.setPosterImg(savePosterJob(file));
        } catch (IOException e) {
            throw new FileStorageException("Failed to save poster image.");
        }

        return jobRepository.saveAndFlush(job);
    }

    @Override
    public Page<Job> getPagingJob(Pageable pageable) {
        return jobRepository.findAll(pageable);
    }

    @Override
    public Job getJobById(Long id) {
        return jobRepository.findById(id)
                .orElseThrow(() -> new NullException(String.format(JOB_NOT_FOUND_MESSAGE, id)));
    }

    public Job updateJob(Long jobId, JobRequest jobDTO, Set<Long> selectedSkillIds, MultipartFile file) {
        Job job = jobRepository.findById(jobId).orElseThrow(
                () -> new NullException(String.format(JOB_NOT_FOUND_MESSAGE, jobId))
        );

        setNewJob(jobDTO, job);

        if (selectedSkillIds != null) {
            Set<Skill> selectedSkills = selectedSkillIds.stream()
                    .map(skillId -> skillRepository.findById(skillId).orElseThrow(
                            () -> new NullException(String.format(SKILL_NOT_FOUND_MESSAGE, skillId))
                    ))
                    .collect(Collectors.toSet());

            job.setSkills(selectedSkills);
        }

        if (file != null) {
            try {
                job.setPosterImg(savePosterJob(file));
            } catch (IOException e) {
                throw new FileStorageException("Failed to save poster image.");
            }
        }

        return jobRepository.save(job);
    }

    private void setNewJob(JobRequest jobDTO, Job job) {
        job.setName(jobDTO.getName());
        job.setExpired(jobDTO.getExpired());
        job.setSalary(jobDTO.getSalary());
        job.setNumberOfRecruits(jobDTO.getNumberOfRecruits());
        job.setType(jobDTO.getType());
        job.setJobPosition(jobDTO.getJobPosition());
        job.setExp(jobDTO.getExp());
        job.setWorkLocation(jobDTO.getWorkLocation());
        job.setDescription(jobDTO.getDescription());
        job.setShortIntroduction(jobDTO.getShortIntroduction());
        job.setRequirement(jobDTO.getRequirement());
        job.setBenefit(jobDTO.getBenefit());
    }

    @Override
    public void deleteJob(Long id) {
        Job job = jobRepository.findById(id)
                .orElseThrow(() -> new NullException(String.format(JOB_NOT_FOUND_MESSAGE, id)));
        Set<Skill> skills = job.getSkills();

        for (Skill skill : skills) {
            skill.getJobs().remove(job);
            skillRepository.save(skill); // Cập nhật lại thông tin kỹ năng trong CSDL
        }

        jobRepository.delete(job);
    }

    @Override
    public Page<Job> filterJobs(String type, String workLocation, String skillName, String searchKeyword, Pageable pageable) {
        return jobRepository.filterJobs(type, workLocation, skillName, searchKeyword, pageable);
    }

    @Override
    public int countJobsNotExpired() {
        LocalDate today = LocalDate.now();
        java.sql.Date sqlDate = java.sql.Date.valueOf(today);
        return jobRepository.countJobsNotExpired(sqlDate);
    }

    @Override
    public List<Job> getAll() {
        return jobRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }
}
