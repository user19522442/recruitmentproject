package com.example.RecruitmentManagement.service;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CandidateServiceImpl implements CandidateService {
    @Autowired
    private CandidateRepository candidateRepository;

    private static final String CANDIDATE_NOT_FOUND_MESSAGE = "Candidate with id %s does not exist";

    @Override
    public Integer getCountCandidate() {
        return candidateRepository.getCountCandidate();
    }

    @Override
    public Page<Candidate> getPagingCandidate(Pageable pageable) {
        return candidateRepository.findAll(pageable);
    }

    @Override
    public void addCandidate(Candidate candidate) {
        if (candidate != null) {
            candidateRepository.save(candidate);
        }
    }

    @Override
    public void updateCandidate(Candidate candidate, Long id) {
        if (candidate != null) {
            Candidate existingCandidate = candidateRepository.findById(id).orElse(null);
            if (existingCandidate != null) {
                existingCandidate.setFirstName(candidate.getFirstName());
                existingCandidate.setLastName(candidate.getLastName());
                existingCandidate.setPhone(candidate.getPhone());
                existingCandidate.setAddress(candidate.getAddress());
                existingCandidate.setGender(candidate.getGender());
                existingCandidate.setDateOfBirth(candidate.getDateOfBirth());
                candidateRepository.save(existingCandidate);
            }
        }
    }

    @Override
    public void deleteCandidate(Long id) {
        Candidate existingCandidate = candidateRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format(CANDIDATE_NOT_FOUND_MESSAGE, id)));
        if (existingCandidate != null)
            candidateRepository.delete(existingCandidate);
    }

    @Override
    public Candidate getCandidateById(Long id) {
        Candidate existingCandidate = candidateRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format(CANDIDATE_NOT_FOUND_MESSAGE, id)));
        return existingCandidate;
    }

    @Override
    public List<Candidate> getAllCandidates() {
        return candidateRepository.findAll();
    }
}
