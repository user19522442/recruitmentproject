package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CandidateService {
    Integer getCountCandidate();
    Page<Candidate> getPagingCandidate(Pageable pageable);
    void addCandidate(Candidate candidate);
    void updateCandidate(Candidate candidate, Long id);
    void deleteCandidate(Long id);
    Candidate getCandidateById(Long id);
    List<Candidate> getAllCandidates();

}
