package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import com.example.RecruitmentManagement.entity.*;
import com.example.RecruitmentManagement.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class AccountServiceImpl implements AccountService{
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private RecruiterRepository recruiterRepository;
    @Autowired
    private InterviewerRepository interviewerRepository;
    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;
    @Autowired
    private EmailService emailService;
    private String host ="https://recruitment-qmd0.onrender.com";

    @Override
    public ApiResponse addAccount(Account account) {
        if (accountRepository.findByEmail(account.getEmail()) != null) {
            return ApiResponse.builder()
                    .statusCode(HttpStatus.CONFLICT.value())
                    .message("Email already exists!")
                    .build();
        } else if (accountRepository.findByUsername(account.getUsername()).isPresent()) {
            return ApiResponse.builder()
                    .statusCode(HttpStatus.CONFLICT.value())
                    .message("Username already exists!")
                    .build();
        } else {
            if (account.getRole()==null)
                account.setRole("ROLE_CANDIDATE");

            account.setPassword(passwordEncoder.encode(account.getPassword())); //Mã hóa mật khẩu
            Account newAccount =  accountRepository.saveAndFlush(account); //Tạo một tài khoản mới
            ConfirmationToken confirmationToken = new ConfirmationToken();
            Date date = new Date();
            confirmationToken.setAccount(newAccount);
            confirmationToken.setCreatedDate(date);
            confirmationToken.setToken(UUID.randomUUID().toString());
            confirmationTokenRepository.save(confirmationToken);
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(account.getEmail());
            mailMessage.setSubject("Complete Registration!");
            mailMessage.setText("To confirm your account, please click here : "
                    + host + "/account/confirm/"+confirmationToken.getToken());

            emailService.sendEmail(mailMessage);

            return ApiResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Account was created successfully")
                    .data(newAccount)
                    .build();
        }
    }

    @Override
    public void confirmAccount(String token) {
        ConfirmationToken existingToken = confirmationTokenRepository.findByToken(token);
        Account account = accountRepository.findByEmail(existingToken.getAccount().getEmail());
        if(existingToken != null && !account.isEnabled())
        {
            account.setEnabled(true);
            accountRepository.save(account);
            switch (account.getRole()) {
                case "ROLE_ADMIN":
                    Admin admin = new Admin();
                    admin.setAccount(account);
                    adminRepository.save(admin);
                    break;
                case "ROLE_RECRUITER":
                    Recruiter recruiter = new Recruiter();
                    recruiter.setAccount(account);
                    recruiterRepository.save(recruiter);
                    break;
                case "ROLE_INTERVIEWER":
                    Interviewer interviewer = new Interviewer();
                    interviewer.setAccount(account);
                    interviewerRepository.save(interviewer);
                    break;
                case "ROLE_CANDIDATE":
                    Candidate candidate = new Candidate();
                    candidate.setAccount(account);
                    candidateRepository.save(candidate);
                    break;
                default:
                    // Xử lý trường hợp không xác định
                    break;
            }
        }
    }

    @Override
    public void updateAccount(Account account, Long id) {
        if (account != null) {
            account.setPassword(passwordEncoder.encode(account.getPassword())); //Mã hóa mật khẩu
            Account existingAccount = accountRepository.findById(id).orElse(null);
            if(existingAccount != null) {
                existingAccount.setEmail(account.getEmail());
                existingAccount.setUsername(account.getUsername());
                existingAccount.setPassword(account.getPassword());
                existingAccount.setRole(account.getRole());
                accountRepository.save(existingAccount); //Lưu lại thông tin vừa cập nhật
            }
        }
    }

    @Override
    public void deleteAccount(Long id) {
        Account account = accountRepository.findById(id).orElse(null);
        if (account != null) {
            accountRepository.delete(account);
        }
    }

    @Override
    public Account getAccount(Long id) {
        Account account = accountRepository.findById(id).orElse(null);
        if (account != null) {
            return account;
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Account with id " + id + " does not exist");
        }
    }

    public Account findAccountByUsername(String username){
        Account account = accountRepository.findByUsername(username).orElse(null);
        if (account != null) {
            return account;
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Account with username " + username + " does not exist");
        }
    }
    @Override
    public List<Account> getListAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public boolean requestPasswordReset(String userEmail) {
        Account account = accountRepository.findByEmail(userEmail);
        if (account != null) {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(account.getEmail());
            mailMessage.setSubject("Reset Password!");
            mailMessage.setText("To generate new password, please click here: "
                    + host + "/account/reset-password/"+account.getConfirmationToken().getToken());
            emailService.sendEmail(mailMessage);
            return true;
        }
        else
            return false;
    }
    public boolean confirmPasswordReset(String token) {
        ConfirmationToken existingToken = confirmationTokenRepository.findByToken(token);
        if(existingToken != null)
            return true;
        else
            return false;
    }
    public void resetPassword(String password, String userEmail) {
        Account account = accountRepository.findByEmail(userEmail);
        account.setPassword(passwordEncoder.encode(password));
        accountRepository.save(account);
    }
}
