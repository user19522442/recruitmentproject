package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Document;
import org.springframework.web.multipart.MultipartFile;

public interface DocumentService {
    Document saveDocument(MultipartFile file, Long id);
    Document getDocument(Long id);
    void updateDocument(Document document, Long id);
}
