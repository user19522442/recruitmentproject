package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.request.JobRequest;
import com.example.RecruitmentManagement.entity.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface JobService {
    Job addJob(JobRequest jobDTO, Set<Long> selectedSkillIds, MultipartFile file);
    Page<Job> getPagingJob(Pageable pageable);
    Job getJobById(Long id);
    Job updateJob(Long jobId, JobRequest jobDTO, Set<Long> selectedSkillIds, MultipartFile file);
    void deleteJob(Long id);
    Page<Job> filterJobs(String type, String workLocation, String skillName, String searchKeyword, Pageable pageable);
    int countJobsNotExpired();
    List<Job> getAll();
}
