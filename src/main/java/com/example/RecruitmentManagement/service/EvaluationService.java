package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Evaluation;

import java.util.List;

public interface EvaluationService {
    public void createEvaluation(Evaluation evaluation);
    public List<Evaluation> getAllEvaluation();
    public Evaluation getEvaluationByID(Long id);
    public boolean deleteEvaluation(Long id);
    public void updateEvaluation(Evaluation evaluation, Long id);
}
