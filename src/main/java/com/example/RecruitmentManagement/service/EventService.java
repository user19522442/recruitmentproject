package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.request.EventRequest;
import com.example.RecruitmentManagement.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface EventService {
    Event addEvent(EventRequest eventDTO, MultipartFile file);
    Event updateEvent(Long id, EventRequest eventDTO, MultipartFile file);
    void removeEvent(Long id);
    Event findById(Long id);
    Integer countEvent();
    Page<Event> getPagingEvent(Pageable pageable);
    List<Event> filterEvent(String name, String field, String address,  int page);
    Integer countCandidateByEvent(Long event_id);

    List<Event> getAll();
}