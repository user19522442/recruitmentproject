package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.response.CandidateApplyResponse;
import com.example.RecruitmentManagement.dto.response.CandidateAssignedResponse;
import com.example.RecruitmentManagement.dto.response.UserBehaviorsResponse;
import com.example.RecruitmentManagement.dto.response.UserStatisticsResponse;
import com.example.RecruitmentManagement.entity.Evaluation;
import com.example.RecruitmentManagement.entity.InterviewProcess;
import com.example.RecruitmentManagement.entity.Interviewer;
import com.example.RecruitmentManagement.repository.CandidateRepository;
import com.example.RecruitmentManagement.repository.InterviewProcessRepository;
import com.example.RecruitmentManagement.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Service
public class InterviewProcessServiceImpl implements InterviewProcessService {

    @Autowired
    private InterviewProcessRepository interviewProcessRepository;

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private JobRepository jobRepository;

    // Get CandidateAssigned List
    @Override
    public Page<CandidateAssignedResponse> getCandidateAssignedList(
            Long id, String keyword, String filterName, Pageable pageable) {
        return interviewProcessRepository.searchAndFilterCandidateAssigned(
                id, keyword, filterName, pageable);

    }

    private static final String CANDIDATE_NOT_FOUND_MESSAGE = "Candidate with id %s does not exist";

    // Count Status
    @Override
    public UserStatisticsResponse countProcessStatus() {
        UserStatisticsResponse result = new UserStatisticsResponse(
                interviewProcessRepository.getUserStatistics("CV apply"),
                interviewProcessRepository.getUserStatistics("Screening CV"),
                interviewProcessRepository.getUserStatistics("Initial Interview"),
                interviewProcessRepository.getUserStatistics("CEO interview"),
                interviewProcessRepository.getUserStatistics("Offered"),
                interviewProcessRepository.getUserStatistics("Hired"));
        return result;
    }

    // Create IP
    @Override
    public void createInterviewProcess(Long jobId, Long candidateId, String url) {
            InterviewProcess savedInterviewProcess = new InterviewProcess();

            Evaluation evaluation = new Evaluation();
            evaluation.setInterviewProcess(savedInterviewProcess);
            savedInterviewProcess.setEvaluation(evaluation);
            savedInterviewProcess.setCandidate(candidateRepository.findById(candidateId).orElse(null));
            savedInterviewProcess.setJob(jobRepository.findById(jobId).orElse(null));

            savedInterviewProcess.setCvInterview(url);
            savedInterviewProcess.setAppliedOn(new Date(System.currentTimeMillis()));
            savedInterviewProcess.setProcessStatus("CV apply");

            interviewProcessRepository.save(savedInterviewProcess);
    }

    // Delete IP
    @Override
    public void deleteInterviewProcess(Long id) {
        InterviewProcess interviewProcess = interviewProcessRepository.findById(id).orElse(null);
        if (interviewProcess != null)
            interviewProcessRepository.delete(interviewProcess);
    }

    @Override
    public void setSchedule(java.util.Date timeSchedule, Interviewer interviewer, Long id) {
        InterviewProcess interviewProcess = interviewProcessRepository.findById(id).orElse(null);
        if(interviewProcess != null){
            interviewProcess.setTimeInterview(timeSchedule);
            interviewProcess.setProcessStatus("waiting for interview");
            interviewProcess.setInterviewer(interviewer);
            interviewProcessRepository.save(interviewProcess);
        }
    }




    @Override
    public InterviewProcess getInterviewProcessById(Long id) {
        return interviewProcessRepository.findById(id).orElse(null);
    }

    @Override
    public void updateProcessStatus(String update, Long id) {
        InterviewProcess interviewProcess = interviewProcessRepository.findById(id).orElse(null);
        if(interviewProcess != null){
            interviewProcess.setProcessStatus(update);
            interviewProcessRepository.save(interviewProcess);
        }
    }

    // Get CandidateApplied List
    @Override
    public Page<CandidateApplyResponse> getCandidateAppliedList(
            Long id, String keyword, String filterName, Pageable pageable) {
        return interviewProcessRepository.searchAndFilterCandidateApplied(
                id, keyword, filterName, pageable);
    }

    @Override
    public Integer countCandidateIsApplying() {
        return interviewProcessRepository.countCandidate();
    }

    @Override
    public int countJobQuantity(Long id) {
        return interviewProcessRepository.countCandidateAppliedToJob(id);
    }

    // Đếm sô lượng người nộp đơn trong 5 ngày qua
    @Override
    public UserBehaviorsResponse lastFiveAppiledList() {
        Date today = new Date(System.currentTimeMillis());
        LocalDate localDate = today.toLocalDate();
        Date yesterday = Date.valueOf(localDate.minusDays(1));
        Date twoDaysAgo = Date.valueOf(localDate.minusDays(2));
        Date threeDaysAgo = Date.valueOf(localDate.minusDays(3));
        Date fourDaysAgo = Date.valueOf(localDate.minusDays(4));
        UserBehaviorsResponse result = new UserBehaviorsResponse(
                interviewProcessRepository.getUserBehaviors(today),
                interviewProcessRepository.getUserBehaviors(yesterday),
                interviewProcessRepository.getUserBehaviors(twoDaysAgo),
                interviewProcessRepository.getUserBehaviors(threeDaysAgo),
                interviewProcessRepository.getUserBehaviors(fourDaysAgo));
        return result;
    }

    // Get Time Interview Schedule
    @Override
    public List<InterviewProcess> getInterviewProcessesByTimeInterview(Integer year, Integer month) {
        return interviewProcessRepository.findByYearAndMonth(year, month);
    }

}
