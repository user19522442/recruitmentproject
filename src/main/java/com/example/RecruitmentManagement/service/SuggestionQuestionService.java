package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.dto.response.SuggestionQuestionResponse;
import com.example.RecruitmentManagement.entity.SuggestionQuestion;

import java.util.List;

public interface SuggestionQuestionService {
    List<SuggestionQuestionResponse> getAllSuggestionQuestions();
    SuggestionQuestion addSuggestionQuestionToSkill(Long skillId, SuggestionQuestion suggestionQuestion);
    SuggestionQuestion updateSuggestionQuestion(Long suggestionQuestionId, SuggestionQuestion updatedSuggestionQuestion);
    void deleteSuggestionQuestion(Long suggestionQuestionId);
    List<SuggestionQuestionResponse> filterSuggestionQuestions(String skillName);
    List<SuggestionQuestionResponse> searchSuggestionQuestions(String keyword);
     List<SuggestionQuestionResponse> getSuggestionQuestionsByJob(Long jobId);
}
