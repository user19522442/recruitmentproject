package com.example.RecruitmentManagement.service;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.entity.Event;
import com.example.RecruitmentManagement.entity.EventParticipant;
import com.example.RecruitmentManagement.repository.CandidateRepository;
import com.example.RecruitmentManagement.repository.EventParticipantRepository;
import com.example.RecruitmentManagement.repository.EventRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class EventParticipantServiceImpl implements EventParticipantService {
    private final EventParticipantRepository eventParticipantRepository;
    private final EventRepository eventRepository;
    private final CandidateRepository candidateRepository;
    String noteFoundMessage = "Could not save file: ";
    String eventMessage = "Event with id ";
    String candidateMessage = " Or Candidate with id ";
    String doesNotExistMessage = " does not exist";

    @Autowired
    public EventParticipantServiceImpl(EventParticipantRepository eventParticipantRepository,
                                       EventRepository eventRepository,
                                       CandidateRepository candidateRepository) {
        this.eventParticipantRepository = eventParticipantRepository;
        this.eventRepository = eventRepository;
        this.candidateRepository = candidateRepository;
    }

    @Override
    public void deleteEventParticipant(Long eventId, Long candidateId) {
        Optional<EventParticipant> eventParticipantOptional = eventParticipantRepository.findByEventIdAndCandidateId(eventId, candidateId);
        if (eventParticipantOptional.isPresent()) {
            EventParticipant eventParticipant = eventParticipantOptional.get();
            eventParticipantRepository.delete(eventParticipant);
        } else {
            throw new EntityNotFoundException(eventMessage + eventId + candidateMessage + candidateId + doesNotExistMessage);
        }
    }

    @Override
    public EventParticipant addEventPariticipant(Long eventId, Long candidateId) {
        // Kiểm tra xem Event và Candidate có tồn tại không
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new EntityNotFoundException("Event with id " + eventId + " not found."));
        Candidate candidate = candidateRepository.findById(candidateId)
                .orElseThrow(() -> new EntityNotFoundException("Candidate with id " + candidateId + " not found."));
        // Kiểm tra xem bản ghi EventParticipant đã tồn tại chưa
        Optional<EventParticipant> existingEventParticipant = eventParticipantRepository.findByEventIdAndCandidateId(eventId, candidateId);
        if (existingEventParticipant.isPresent()) {
            throw new EntityNotFoundException("EventParticipant with event_id " + eventId + " and candidate_id " + candidateId + " already exists.");
        }
        // Tạo mới bản ghi EventParticipant
        EventParticipant eventParticipant = new EventParticipant();
        eventParticipant.setEvent(event);
        eventParticipant.setCandidate(candidate);
        return eventParticipantRepository.saveAndFlush(eventParticipant);

    }

}
