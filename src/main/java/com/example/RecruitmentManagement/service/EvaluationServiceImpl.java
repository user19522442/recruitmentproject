package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Evaluation;
import com.example.RecruitmentManagement.repository.EvaluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationServiceImpl implements EvaluationService{
    @Autowired
    private EvaluationRepository evaluationRepository;
    /*create an evaluation*/
    @Override
    public void createEvaluation(Evaluation evaluation) {
        if (evaluation != null){
            evaluationRepository.save(evaluation);
        }
    }

    /*get all evaluation*/
    @Override
    public List<Evaluation> getAllEvaluation() {
        return evaluationRepository.findAll();
    }

    /*get evaluation by id*/
    @Override
    public Evaluation getEvaluationByID(Long id) {
        return evaluationRepository.findById(id).orElseThrow(null);
    }

    /*delete an evaluation*/
    @Override
    public boolean deleteEvaluation(Long id) {
        Evaluation deleteEvaluation = evaluationRepository.findById(id).orElse(null);
        if (deleteEvaluation != null) {
            evaluationRepository.deleteById(id);
            return true;
        }
        return false;
    }

    /*update an evaluation*/
    @Override
    public void updateEvaluation(Evaluation evaluation, Long id) {
        if (evaluation != null) {
            Evaluation existEvaluation = evaluationRepository.findById(id).orElseThrow(null);
            existEvaluation.setScore(evaluation.getScore());
            existEvaluation.setNote(evaluation.getNote());
            existEvaluation.setPass(evaluation.isPass());
            evaluationRepository.save(existEvaluation);

        }
    }


}
