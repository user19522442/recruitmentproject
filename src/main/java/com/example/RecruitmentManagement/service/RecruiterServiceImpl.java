package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Recruiter;
import com.example.RecruitmentManagement.repository.RecruiterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecruiterServiceImpl implements RecruiterService{
    @Autowired
    private RecruiterRepository recruiterRepository;
    @Override
    public void createRecruiter(Recruiter recruiter) {
        // Triển khai logic tạo Recruiter ở đây
        recruiterRepository.save(recruiter);
    }

    @Override
    public void updateRecruiter(Recruiter recruiter, Long recruiterId) {
        // Triển khai logic cập nhật Recruiter ở đây
        Recruiter existingRecruiter = recruiterRepository.findById(recruiterId)
                .orElseThrow(() -> new IllegalArgumentException("Recruiter not found with ID: " + recruiterId));

        existingRecruiter.setFirstName(recruiter.getFirstName());
        existingRecruiter.setLastName(recruiter.getLastName());
        existingRecruiter.setGender(recruiter.getGender());
        existingRecruiter.setDateOfBirth(recruiter.getDateOfBirth());
        existingRecruiter.setPhone(recruiter.getPhone());
        existingRecruiter.setAddress(recruiter.getAddress());

        recruiterRepository.save(existingRecruiter);
    }

    @Override
    public void deleteRecruiter(Long recruiterId) {
        // Triển khai logic xóa Recruiter ở đây
        recruiterRepository.deleteById(recruiterId);
    }

    @Override
    public Recruiter getRecruiter(Long recruiterId) {
        // Triển khai logic lấy thông tin Recruiter theo ID ở đây
        return recruiterRepository.findById(recruiterId)
                .orElseThrow(() -> new IllegalArgumentException("Recruiter not found with ID: " + recruiterId));
    }

    @Override
    public List<Recruiter> getListRecruiters() {
        // Triển khai logic lấy danh sách tất cả Recruiter ở đây
        return recruiterRepository.findAll();
    }

    @Override
    public Integer getCountRecruiters() {
        return recruiterRepository.getCountRecruiters();
    }

    @Override
    public List<Recruiter> searchRecruitersByName(String keyword) {
        return recruiterRepository.searchRecruitersByName(keyword);
    }
}
