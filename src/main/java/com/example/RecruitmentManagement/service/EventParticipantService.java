package com.example.RecruitmentManagement.service;
import com.example.RecruitmentManagement.dto.EventParticipantDTO;
import com.example.RecruitmentManagement.entity.EventParticipant;
public interface EventParticipantService {
    void deleteEventParticipant(Long eventId, Long candidateId);

    EventParticipant addEventPariticipant( Long eventId, Long candidateId);
}