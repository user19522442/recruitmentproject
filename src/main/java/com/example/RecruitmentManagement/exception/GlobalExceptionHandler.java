package com.example.RecruitmentManagement.exception;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    // Check Null
    @ExceptionHandler(NullException.class)
    public ResponseEntity<ApiResponse> handleNullException(NullException ex) {
        ApiResponse defaultResponse = ApiResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build();
        return ResponseEntity.badRequest().body(defaultResponse);
    }

    // Not found
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ApiResponse> handleResourceNotFoundException(ResourceNotFoundException ex) {
        ApiResponse defaultResponse = ApiResponse.builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .message(ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(defaultResponse);
    }

    // Duplicate
    @ExceptionHandler(DuplicateException.class)
    public ResponseEntity<ApiResponse> handleDuplicateKeyException(DuplicateException ex) {
        ApiResponse defaultResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CONFLICT.value())
                .message(ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.CONFLICT).body(defaultResponse);
    }


    @ExceptionHandler(WarningException.class)
    public ResponseEntity<ApiResponse> handleWarningException(WarningException ex) {
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }

    // Handle BadCredentialsException return DefaultResponse
//    @ExceptionHandler(BadCredentialsException.class)
//    public ResponseEntity<DefaultResponse> handleBadCredentialsException(BadCredentialsException ex) {
//        DefaultResponse response = DefaultResponse.builder()
//                .statusCode(HttpStatus.UNAUTHORIZED.value())
//                .message(ex.getMessage())
//                .build();
//        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
//    }

    // config exception validate
//    @ExceptionHandler(BindException.class)
//    public ResponseEntity<DefaultResponse> handleBindException(BindException ex) {
//        DefaultResponse response = DefaultResponse.builder()
//                .statusCode(HttpStatus.BAD_REQUEST.value())
//                .message(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage())
//                .build();
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
//    }

    // Handle others exception not define
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> handleException(Exception ex) {
        ApiResponse response = ApiResponse.builder()
                .statusCode(500)
                .message(ex.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }
}
