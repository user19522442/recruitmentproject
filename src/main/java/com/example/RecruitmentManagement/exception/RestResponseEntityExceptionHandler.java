package com.example.RecruitmentManagement.exception;

import com.example.RecruitmentManagement.dto.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<Object> handleAccessDeniedException(
            Exception ex, WebRequest request) {
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .message("Access Denied")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
