package com.example.RecruitmentManagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "skills")
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String skillName;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "skills")
    @JsonIgnore
    private Set<Job> jobs = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "skill")
    private List<SuggestionQuestion> suggestionQuestionList;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Skill skill = (Skill) o;
        return Objects.equals(id, skill.id) && Objects.equals(skillName, skill.skillName)
                && Objects.equals(jobs, skill.jobs)
                && Objects.equals(suggestionQuestionList, skill.suggestionQuestionList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, skillName);
    }
}

