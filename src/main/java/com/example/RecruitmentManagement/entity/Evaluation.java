package com.example.RecruitmentManagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "evaluations")
public class Evaluation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String score;
    private String note;
    private boolean pass; // pass and fail

<<<<<<< HEAD:src/main/java/com/example/RecruitmentManagement/entity/Evaluation.java
=======
    @JsonIgnore
>>>>>>> 7453a3c44a13eb8351cf0b3fa5c5873482a8f2b9:RecruitmentManagement/src/main/java/com/example/RecruitmentManagement/entity/Evaluation.java
    @OneToOne(mappedBy = "evaluation", cascade = CascadeType.ALL)
    private InterviewProcess interviewProcess;

}
