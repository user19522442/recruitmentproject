package com.example.RecruitmentManagement.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "Interview_Process")
public class InterviewProcess {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    private java.sql.Date appliedOn;
    private String processStatus;
    private java.util.Date timeInterview;
    private String cvInterview;

    @OneToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @JsonIgnore
<<<<<<< HEAD:src/main/java/com/example/RecruitmentManagement/entity/InterviewProcess.java
=======
    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;

>>>>>>> 7453a3c44a13eb8351cf0b3fa5c5873482a8f2b9:RecruitmentManagement/src/main/java/com/example/RecruitmentManagement/entity/InterviewProcess.java
    @OneToOne
    @JoinColumn(name = "evaluation_id")
    private Evaluation evaluation;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "interviewer_id")
    private Interviewer interviewer;
}
