package com.example.RecruitmentManagement.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "jobs")
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    private Date expired;
    private int salary;
    private int numberOfRecruits;
    private int candidateIsApply; // thêm này vào
    private String type;
    private String jobPosition;
    private String exp;
    private String workLocation;
    private String description;
    private String shortIntroduction;
    private String requirement;
    private String benefit;
    private String posterImg;

<<<<<<< HEAD:src/main/java/com/example/RecruitmentManagement/entity/Job.java
=======
    @OneToMany(mappedBy = "job",cascade = CascadeType.ALL)
    private List<InterviewProcess> interviewProcessList;
>>>>>>> 7453a3c44a13eb8351cf0b3fa5c5873482a8f2b9:RecruitmentManagement/src/main/java/com/example/RecruitmentManagement/entity/Job.java

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(name = "Job_Skill", joinColumns = @JoinColumn(name = "job_id"), inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills = new HashSet<>();

<<<<<<< HEAD:src/main/java/com/example/RecruitmentManagement/entity/Job.java
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    private List<InterviewProcess> interviewProcessList;
=======
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Job job = (Job) o;
        return salary == job.salary && numberOfRecruits == job.numberOfRecruits && Objects.equals(id, job.id)
                && Objects.equals(name, job.name) && Objects.equals(expired, job.expired)
                && Objects.equals(type, job.type) && Objects.equals(jobPosition, job.jobPosition)
                && Objects.equals(exp, job.exp) && Objects.equals(workLocation, job.workLocation)
                && Objects.equals(description, job.description)
                && Objects.equals(shortIntroduction, job.shortIntroduction)
                && Objects.equals(requirement, job.requirement) && Objects.equals(benefit, job.benefit)
                && Objects.equals(posterImg, job.posterImg) && Objects.equals(skills, job.skills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, expired, salary, numberOfRecruits, type, jobPosition, exp, workLocation,
                description, shortIntroduction, requirement, benefit, posterImg, skills);
    }

>>>>>>> 7453a3c44a13eb8351cf0b3fa5c5873482a8f2b9:RecruitmentManagement/src/main/java/com/example/RecruitmentManagement/entity/Job.java
}
